(in-package :cl)
(defpackage :anathema-doom
  (:use :cl)
  (:local-nicknames (:at :anathema)
                    (:ats :anathema.style))
  (:export :molokai :*molokai*
           :doom-one :*doom-one*))
