(in-package :anathema-doom)

(at:define-singleton doom-one (at:theme) ()
  (:default-initargs :name "Doom One"))

;; default style
(defmethod at:foreground-ink-1
    ((style ats:default) (theme doom-one) medium)
  (at:make-color "bbc2cf"))
(defmethod background-ink-1
    ((style ats:default) (theme doom-one) medium)
  (at:make-color "282c34"))

;; highlight
(defmethod at:foreground-ink-1
    ((style ats:highlight) (theme doom-one) medium)
  (at:make-color "51afef"))

;; headings
(defmethod at:foreground-ink-1
    ((style ats:heading-1) (theme doom-one) medium)
  (at:make-color "51afef"))
(defmethod at:foreground-ink-1
    ((style ats:heading-2) (theme doom-one) medium)
  (at:make-color "c678dd"))
(defmethod at:foreground-ink-1
    ((style ats:heading-3) (theme doom-one) medium)
  (at:make-color "a9a1e1"))
(defmethod at:foreground-ink-1
    ((style ats:heading-4) (theme doom-one) medium)
  (at:make-color "7cc3f3"))
(defmethod at:foreground-ink-1
    ((style ats:heading-5) (theme doom-one) medium)
  (at:make-color "d499e5"))
(defmethod at:foreground-ink-1
    ((style ats:heading-6) (theme doom-one) medium)
  (at:make-color "a8d7f7"))
(defmethod at:foreground-ink-1
    ((style ats:heading-7) (theme doom-one) medium)
  (at:make-color "e2bbee"))
#+(or)
(defmethod at:foreground-ink-1
    ((style ats:heading-8) (theme doom-one) medium)
  (at:make-color "A6E22E"))

;; graph bars
(defmethod at:foreground-ink-1
    ((style ats:graph-bar) (theme doom-one) medium)
  clim:+black+)

(defmethod background-ink-1
    ((style ats:graph-bar) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-1) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-1* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-2) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-2* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-3) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-3* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-4) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-4* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-5) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-5* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-6) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-6* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-7) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-7* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar-8) (theme doom-one) medium)
  (at:foreground-ink-1 ats:*heading-8* theme medium))

(defmethod background-ink-1
    ((style ats:graph-bar) (theme doom-one) medium)
  clim:+white+)

(defmethod outline-ink-1
    ((style ats:graph-bar) (theme doom-one) medium)
  clim:+black+)
