(defsystem     "anathema-doom"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Doom-Emacs themes defined using the clim-anathema library"
  :serial      t
  :depends-on  (:anathema)
  :components  ((:file "package")
                (:file "doom-molokai")
                (:file "doom-one")))
