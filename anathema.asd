(defsystem     "anathema"
  :version     "0.0.1"
  :license     "Unlicense"
  :author      "contrapunctus <contrapunctus at disroot dot org>"
  :description "Color theme library for CLIM applications"
  :serial      t
  :depends-on  (:alexandria :cl-colors2 :serapeum :clim :closer-mop :split-sequence)
  :components  ((:module "src/"
                 :components ((:file "packages")
                              (:file "core")
                              (:file "styles")
                              (:file "default")))))
