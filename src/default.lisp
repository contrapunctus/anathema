(in-package :anathema)

(defmethod foreground-ink-1 (style theme medium)
  clim:+black+)

(defmethod background-ink-1 (style theme medium)
  clim:+white+)

(defmethod outline-ink-1 (style theme medium)
  clim:+black+)

(defmethod line-style-1 (style theme medium)
  (clim:make-line-style))

(defmethod text-style-1 (style theme medium)
  clim:*default-text-style*)

(defmethod text-style-1 ((style anathema.style:code) theme medium)
  (clim:make-text-style :fix nil nil))

(defmethod foreground-ink-1 ((style anathema.style:graph-bar) theme medium)
  clim:+black+)
(defmethod background-ink-1 ((style anathema.style:graph-bar) theme medium)
  clim:+white+)
(defmethod outline-ink-1 ((style anathema.style:graph-bar) theme medium)
  clim:+black+)
