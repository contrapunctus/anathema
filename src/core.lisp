(in-package :anathema)

;; Theme definition protocol
(defclass style () ()
  (:documentation
   "Protocol class representing a common application object which may
be displayed differently by a theme."))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun earmuff-symbol (symbol)
    (intern (concatenate 'string "*" (symbol-name symbol) "*"))))

(defmacro define-singleton (name direct-superclasses direct-slots &rest options)
  "Like `defclass', but also define a `*<name>*' special variable
holding an instance of the class."
  `(progn
     (defclass ,name ,direct-superclasses ,direct-slots ,@options)
     (defvar ,(earmuff-symbol name)
       (make-instance (quote ,name)))))

(defmacro define-style (name direct-superclasses direct-slots &rest options)
  "Define an instance of `anathema:style'.

Like `defclass', but also define a `*<name>*' special variable holding
an instance of the class.

If DIRECT-SUPERCLASSES do not contain `anathema:style' or a subclass
of the same, `anathema:style' is added to the DIRECT-SUPERCLASSES."
  `(define-singleton ,name
       ,(if (loop for class in direct-superclasses
                    thereis (subtypep (find-class class)
                                      (find-class 'anathema:style)))
            direct-superclasses
            (cons 'anathema:style direct-superclasses))
     ,direct-slots ,@options))

(defclass theme ()
  ((%name :initarg :name
          :accessor name
          :initform (error "No theme name supplied"))
   (%description :initarg :description
                 :accessor description
                 :type (or null string)))
  (:documentation "Protocol base class for all themes."))

(defmethod print-object ((theme theme) stream)
  (print-unreadable-object (theme stream :type t)
    (format stream "~A" (name theme))))

(defmethod make-color ((hex string))
  (let ((color (color:as-rgb hex)))
    (s:~> (list (color:rgb-red color)
                (color:rgb-green color)
                (color:rgb-blue color))
          (mapcar #'float _)
          (apply #'clim:make-rgb-color _))))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defmacro define-theme (name direct-superclasses direct-slots &rest options)
    "Convenience macro for defining themes.

Like `defclass', but also define a special variable `*NAME*'
containing an instance of this subclass.

OPTIONS may include a (:SPECIFIERS &REST SPECIFIERS) form, which is
used to define methods on `anathema:foreground-ink-1',
`anathema:background-ink-1', `anathema:outline-ink-1',
`anathema:line-style-1', and `anathema:text-style-1'.

Each specifier must be a list in the form

(STYLE [KEYWORD BODY]+)

where

STYLE is the name of a subclass of `anathema:style', or T to accept
any Lisp object (like a `defmethod' specializer),

KEYWORD is one of `:foreground-ink', `:background-ink',
`:outline-ink', `:line-style', or `:text-style',

and BODY may be

a string (which is assumed to be a color as a hexadecimal value, and
passed to `anathema:make-color'),

a variable containing such a string,

or any number of forms which return the desired instance of
`clim:ink', `clim:line-style', or `clim:text-style' for this
combination of ACCESSOR, STYLE, and theme.

Within BODY, the variables STYLE and MEDIUM are bound to the values of
STYLE and MEDIUM, and the variable THEME is bound to the instance of
the new subclass."
    `(progn
       (define-singleton ,name ,direct-superclasses ,direct-slots
         ;; Pass all options that aren't `:specifiers' to `defclass'
         ,@(loop for option in options
                 unless (eq (first option) :specifiers)
                   collect option))
       ;; Find the first (:SPECIFIERS ...) form
       ,@(loop
           with specifiers = (rest (loop for option in options
                                         when (eq (first option) :specifiers)
                                           return option))
           ;; Define a method for each specifier
           for specifier in specifiers
           for style = (first specifier)
           append
           (loop
             for body in (ss:split-sequence-if #'keywordp (rest specifier)
                                               :remove-empty-subseqs t)
             for keyword in (remove-if-not #'keywordp (rest specifier))
             collect
             `(defmethod ,(intern (format nil "~A-1" (symbol-name keyword))
                                  (find-package 'anathema))
                  ((style ,style) (theme ,name) (medium t))
                ;; Use `make-color' if body is a string, or a variable containing a string
                ,(let ((first (first body)))
                   (cond ((rest body)
                          `(progn ,@body))
                         ((stringp first)
                          `(anathema:make-color ,first))
                         ((and (symbolp first)
                               (boundp first)
                               (stringp (symbol-value first)))
                          `(anathema:make-color ,(symbol-value first)))))))))))

;; High-level theme accessors
(defvar *current-theme* nil
  "The instance of `anathema:theme' currently being used.

A value of NIL means the default theme is in effect.

Affects high-level theme accessors, i.e. `style-foreground-ink',
`style-background-ink', `style-outline-ink', `style-line-style', and
`style-text-style'.")

(defmacro with-style ((medium style &rest options) &body body)
  "Like `clim:with-drawing-options', but with :INK, :LINE-STYLE, and
:TEXT-STYLE bound to the corresponding values for STYLE in the
`*current-theme*'.

`anathema:style-foreground-ink' is used for :INK."
  `(clim:with-drawing-options
       (,medium :ink (style-foreground-ink ,style)
                :line-style (style-line-style ,style)
                :text-style (style-text-style ,style)
                ,@options)
     ,@body))

(defun style-foreground-ink (style)
  "Return the foreground ink for STYLE in the `*current-theme*'."
  (foreground-ink-1 style
                    *current-theme*
                    clim:*application-frame*))

(defun style-background-ink (style)
  "Return the background ink for STYLE in the `*current-theme*'."
  (background-ink-1 style
                    *current-theme*
                    clim:*application-frame*))

(defun style-outline-ink (style)
  "Return the outline ink for STYLE in the `*current-theme*'."
  (outline-ink-1 style
                 *current-theme*
                 clim:*application-frame*))

(defun style-line-style (style)
  "Return the line style for STYLE in the `*current-theme*'."
  (line-style-1 style
                *current-theme*
                clim:*application-frame*))

(defun style-text-style (style)
  "Return the text style for STYLE in the `*current-theme*'."
  (text-style-1 style
                *current-theme*
                clim:*application-frame*))

(defun heading-foreground-inks ()
  "Return a circular list of all defined heading colors."
  (loop :for style :in (anathema.style:heading-styles)
        :for ink = (style-foreground-ink style)
        :when ink :collect it :into colors
        :finally (return (apply #'al:circular-list colors))))

(defun bar-background-inks ()
  "Return a circular list of all defined heading colors."
  (loop :for style :in (anathema.style:graph-bar-styles)
        :for ink = (style-background-ink style)
        :when ink :collect it :into inks
        :finally (return (apply #'al:circular-list inks))))

;; Low-level theme accessors
(defgeneric foreground-ink-1 (style theme medium)
  (:documentation "Return concrete foreground ink based on STYLE, THEME, and MEDIUM."))

(defgeneric background-ink-1 (style theme medium)
  (:documentation "Return concrete background ink based on STYLE, THEME, and MEDIUM."))

(defgeneric outline-ink-1 (style theme medium)
  (:documentation "Return concrete outline ink based on STYLE, THEME, and MEDIUM."))

(defgeneric line-style-1 (style theme medium)
  (:documentation "Return line style based on STYLE, THEME, and MEDIUM."))

(defgeneric text-style-1 (style theme medium)
  (:documentation "Return text style based on STYLE, THEME, and MEDIUM."))
