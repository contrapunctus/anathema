(in-package :cl)
(defpackage :anathema
  (:use :cl)
  (:local-nicknames (:cm :closer-mop)
                    (:ss :split-sequence)
                    (:al :alexandria)
                    (:color :cl-colors2)
                    (:s :serapeum))

  ;; Theme definition protocol
  (:export #:theme #:define-theme
           #:style #:define-style
           #:make-color
           #:define-singleton)

  ;; High-level theme accessors
  (:export #:*current-theme*
           #:style-foreground-ink
           #:style-background-ink
           #:style-outline-ink
           #:style-line-style
           #:style-text-style
           #:heading-foreground-inks
           #:bar-background-inks)

  ;; Low-level theme accessors
  (:export #:foreground-ink-1
           #:background-ink-1
           #:outline-ink-1
           #:line-style-1
           #:text-style-1
           #:with-style))

(defpackage :anathema.style
  (:import-from :cl #:defvar #:defclass #:progn #:make-instance)
  (:import-from :anathema #:style #:define-singleton)
  ;; instances
  (:export #:*default* #:*selection* #:*region* #:*highlight*

           #:*tag* #:*property* #:*pathname* #:*link*

           #:*heading* #:heading-styles
           #:*heading-1* #:*heading-2* #:*heading-3* #:*heading-4*
           #:*heading-5* #:*heading-6* #:*heading-7* #:*heading-8*

           #:*graph-bar* #:graph-bar-styles
           #:*graph-bar-1* #:*graph-bar-2* #:*graph-bar-3* #:*graph-bar-4*
           #:*graph-bar-5* #:*graph-bar-6* #:*graph-bar-7* #:*graph-bar-8*

           #:*code*
           #:*comment*
           #:*constant*
           #:*variable*
           #:*function*
           #:*method*
           #:*keyword*
           #:*operator*
           #:*type*
           #:*number*
           #:*string*

           #:*error* #:*warning* #:*success*

           #:*addition* #:*modification* #:*deletion*)
  ;; classes
  (:export #:default #:selection #:region #:highlight

           #:tag #:property #:pathname #:link

           #:heading
           #:heading-1 #:heading-2 #:heading-3 #:heading-4
           #:heading-5 #:heading-6 #:heading-7 #:heading-8

           #:graph-bar
           #:graph-bar-1 #:graph-bar-2 #:graph-bar-3 #:graph-bar-4
           #:graph-bar-5 #:graph-bar-6 #:graph-bar-7 #:graph-bar-8

           #:code
           #:comment
           #:constant
           #:variable
           #:function
           #:method
           #:keyword
           #:operator
           #:type
           #:number
           #:string

           #:error #:warning #:success

           #:addition #:modification #:deletion))
